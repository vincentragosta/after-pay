<section class="content-section content-section--arch anchor content-section--has-bg content-section--width-full">
    <span class="content-background-image">
        <span class="content-background-image__images">
            <span class="content-background-image__img content-background-image__img--desktop" style="background-image: url('http://afterpay.test/wp-content/uploads/2020/10/Hero-1400x622.jpg');"></span>
        </span>
    </span>
    <div class="container-fluid content-section__container">
        <div class="content-row row">
            <div class="content-column col-md-12 text--center align-self-center content-column--last">
                <div class="content-column__inner">
                    <div class="vc-single-image vc-single-image--center">
                        <figure class="vc-single-image__figure image-loaded">
                            <img class="lazy-image-loaded vc-single-image__image in-view" src="http://afterpay.test/wp-content/uploads/2020/10/Arch.png" alt="frame-brick" width="880" height="520">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content-section content-section--arch anchor content-section--has-bg content-section--width-full">
    <span class="content-background-image">
        <span class="content-background-image__images">
            <span class="content-background-image__img content-background-image__img--desktop" style="background-image: url('http://afterpay.test/wp-content/uploads/2020/10/Hero-1400x622.jpg');"></span>
        </span>
    </span>
    <div class="container-fluid content-section__container">
        <div class="content-row row">
            <div class="content-column col-md-12 text--center align-self-center content-column--last">
                <div class="content-column__inner">
                    <div class="vc-single-image vc-single-image--center">
                        <figure class="vc-single-image__figure image-loaded">
                            <img class="lazy-image-loaded vc-single-image__image in-view" src="http://afterpay.test/wp-content/uploads/2020/10/Arch.png" alt="frame-brick" width="880" height="520">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
