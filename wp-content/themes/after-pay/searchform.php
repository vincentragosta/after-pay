<?php

use Backstage\SetDesign\Icon\IconView;
use ChildTheme\Options\GlobalOptions;

$label = !empty($placeholder_text = GlobalOptions::searchPlaceholderText()) ? $placeholder_text : 'Search Afterpay.com:';
?>

<form role="search" method="get" id="searchform" class="searchform" action="">
    <label class="screen-reader-text" for="s"><?= $label; ?></label>
    <?= new IconView(['icon_name' => 'search']); ?>
    <input type="text" value="" name="s" id="s" <?= !empty($placeholder_text) ? 'placeholder="' . $placeholder_text . '"' : ''; ?>>
    <input type="submit" id="searchsubmit" class="button" value="<?= !empty($search_button_text = GlobalOptions::searchButtonText()) ? $search_button_text : 'Search'; ?>">
</form>
