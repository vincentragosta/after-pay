<?php

use Backstage\SetDesign\Icon\IconView;
use Backstage\SetDesign\SocialIcons\SocialIcon;
use Backstage\Util;
use Backstage\View\Link;
use ChildTheme\Options\GlobalOptions;

?>

<footer class="footer-nav">
    <div class="footer-nav__top">
        <div class="footer-nav__container container">
            <?php if (has_nav_menu('sub_footer_navigation')): ?>
                <?php if (!empty($sub_footer_navigation = GlobalOptions::menuItems('sub_footer_navigation'))): ?>
                    <ul class="footer-nav__list">
                        <?php foreach($sub_footer_navigation as $MenuItem): ?>
                            <?php if (empty((int)$MenuItem->menu_item_parent)): ?>
                                <li class="footer-nav__item footer-nav__item--heading">
                                    <h2 class="footer-nav__heading heading"><?= $MenuItem->title; ?></h2>
                                </li>
                            <?php else: ?>
                                <li class="footer-nav__item">
                                    <a href="<?= $MenuItem->url; ?>"><?= $MenuItem->title; ?></a>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            <?php endif; ?>
            <?php if (has_nav_menu('sub_footer_navigation_2')): ?>
                <?php if (!empty($sub_footer_navigation_2 = GlobalOptions::menuItems('sub_footer_navigation_2'))): ?>
                    <ul class="footer-nav__list">
                        <?php foreach($sub_footer_navigation_2 as $MenuItem): ?>
                            <?php if (empty((int)$MenuItem->menu_item_parent)): ?>
                                <li class="footer-nav__item footer-nav__item--heading">
                                    <h2 class="footer-nav__heading heading"><?= $MenuItem->title; ?></h2>
                                </li>
                            <?php else: ?>
                                <li class="footer-nav__item">
                                    <a href="<?= $MenuItem->url; ?>"><?= $MenuItem->title; ?></a>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            <?php endif; ?>
            <?php if (!empty($social_icons = GlobalOptions::socialIcons())): ?>
                <div class="footer-nav__social-icons-container">
                    <h2 class="heading">Connect</h2>
                    <ul class="footer-nav__social-icons list--inline">
                        <?php foreach($social_icons as $SocialIcon): /** @var SocialIcon $SocialIcon */?>
                            <li class="footer-nav__social-item">
                                <a href="<?= $SocialIcon->getUrl(); ?>" target="_blank" class="button--with-icon-vertical">
                                    <?= new IconView(['icon_name' => $SocialIcon->getName()]); ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
            <div class="footer-nav__apps-container">
                <h2 class="footer-nav__app-heading heading">The Afterpay App</h2>
                <ul class="footer-nav__apps list--inline">
                    <?php if (($AppleStoreImage = GlobalOptions::appleStoreImage()) instanceof WP_Image): ?>
                        <li class="footer-nav__app-item">
                            <?php if ($apple_store_url = GlobalOptions::appleStoreUrl()): ?>
                                <?= new Link($apple_store_url, $AppleStoreImage->css_class('footer-nav__app-image')); ?>
                            <?php else: ?>
                                <?= $AppleStoreImage->css_class('footer-nav__app-image'); ?>
                            <?php endif; ?>
                        </li>
                    <?php endif; ?>
                    <?php if (($GooglePlayImage = GlobalOptions::googlePlayImage()) instanceof WP_Image): ?>
                        <li class="footer-nav__app-item">
                            <?php if ($google_play_url = GlobalOptions::googlePlayUrl()): ?>
                                <?= new Link($google_play_url, $GooglePlayImage->css_class('footer-nav__app-image')); ?>
                            <?php else: ?>
                                <?= $GooglePlayImage->css_class('footer-nav__app-image'); ?>
                            <?php endif; ?>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-nav__bottom">
        <div class="footer-nav__container container">
            <div class="footer-nav__menu-legal-container">
                <?php if (has_nav_menu('footer_navigation')): ?>
                    <?php if (!empty($footer_navigation = GlobalOptions::menuItems('footer_navigation'))): ?>
                        <ul class="footer-nav__menu">
                            <?php foreach($footer_navigation as $MenuItem): ?>
                                <li class="footer-nav__menu-item">
                                    <a href="<?= $MenuItem->url; ?>"><?= $MenuItem->title; ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if (!empty($legal_text = GlobalOptions::legalText())): ?>
                    <div class="footer-nav__legal">
                        <?= $legal_text; ?>
                    </div>
                <?php endif; ?>
            </div>
            <?php if (($logo = GlobalOptions::brandLogo()) instanceof WP_Image): ?>
                <div class="footer-nav__logo-container">
                    <?= $logo->css_class('footer-nav__logo'); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</footer>
