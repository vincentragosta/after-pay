<?php

use Backstage\SetDesign\Icon\IconView;
use Backstage\View\Link;
use ChildTheme\Options\GlobalOptions;
?>

<header class="header-nav">
    <div class="header-nav__top-bar">
        <div class="header-nav__container container">
            <div class="header-nav__logo-container">
                <?php if (($logo = GlobalOptions::brandLogo()) instanceof WP_image): ?>
                    <a href="<?= GlobalOptions::MAIN_SITE_URL; ?>" class="header-nav__picture-link">
                        <picture class="header-nav__picture">
                            <?php if (($logo_condensed = GlobalOptions::logoCondensed()) instanceof WP_image): ?>
                                <source srcset="<?= $logo_condensed->url; ?>" media="(max-width: 767px)">
                            <?php endif; ?>
                            <?= $logo->css_class('header-nav__logo'); ?>
                        </picture>
                    </a>
                <?php endif; ?>
                <?= new IconView(['icon_name' => 'arrow', 'direction' => 'down']); ?>
                <?= new IconView(['icon_name' => 'close']); ?>
            </div>
            <div class="header-nav__search-container">
                <?= get_search_form(); ?>
            </div>
            <?php if (has_nav_menu('sub_header_navigation')): ?>
            <div class="header-nav__sub-menu-container">
                    <ul class="header-nav__sub-menu">
                        <?php if (!empty($sub_header_menu_items = GlobalOptions::menuItems('sub_header_navigation'))): ?>
                            <?php foreach($sub_header_menu_items as $MenuItem): ?>
                                <li class="header-nav__sub-menu-item"><a href="<?= $MenuItem->url; ?>"><?= $MenuItem->title; ?></a></li>
                            <?php endforeach; ?>
                            <?php if (!empty($log_in_link = GlobalOptions::logInLink()) && isset($log_in_link['url']) && isset($log_in_link['title'])): ?>
                                <li class="header-nav__sub-menu-item header-nav__sub-menu-item--button"><?= Link::createFromField($log_in_link)->class('header-nav__log-in button'); ?></li>
                            <?php endif; ?>
                            <?php if (!empty($signup_link = GlobalOptions::signUpLink()) && isset($signup_link['url']) && isset($signup_link['title'])): ?>
                                <li class="header-nav__sub-menu-item header-nav__sub-menu-item--button"><?= Link::createFromField($signup_link)->class('header-nav__signin button button--inverted'); ?></li>
                            <?php endif; ?>
                        <?php endif; ?>
                    </ul>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="header-nav__bottom-bar">
        <div class="header-nav__container container">
            <?php if (has_nav_menu('sub_header_navigation')): ?>
                <ul class="header-nav__menu">
                    <?php if (!empty($primary_menu_items = GlobalOptions::menuItems('primary_navigation'))): ?>
                        <?php foreach($primary_menu_items as $MenuItem): ?>
                            <li class="header-nav__menu-item"><a href="<?= $MenuItem->url; ?>"><?= $MenuItem->title; ?></a></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            <?php endif; ?>
        </div>
    </div>
</header>
