<?php
/**
 * Template Name: Galleria
 */

use Backstage\Util;
use ChildTheme\Components\Window\WindowView;
use ChildTheme\Controller\AmplitudeController;
use ChildTheme\GiftGuide\GiftGuide;
use ChildTheme\Merchant\Merchant;
use ChildTheme\Options\GlobalOptions;
use ChildTheme\Templates\Galleria;

$Galleria = Galleria::createFromGlobal();

//$amplitude = Zumba\Amplitude\Amplitude::getInstance();
//$amplitude->init('3280fbc3e2563a5279383a0aae55d919');
//$amplitude->queueEvent('Galleria - Page - View', [
//    'path' => AmplitudeController::getPageUrl(),
//    'country' => 'US'
//]);
//$amplitude->setOptOut(true);
//var_dump($amplitude);
?>

<section class="galleria-sign container">
    <img class="galleria-sign__image" src="<?= Util::getAssetPath('images/sign.png'); ?>" alt="Afterpay Sign"/>
    <img class="galleria-sign__image galleria-sign__image--off" src="<?= Util::getAssetPath('images/sign-off.png'); ?>"
         alt="Afterpay Unlit Sign"/>
</section>
<section class="galleria-arch container">
    <picture>
        <source srcset="<?= Util::getAssetPath('images/arch-top-mobile.jpg'); ?>" media="(max-width: 575px)">
        <img src="<?= Util::getAssetPath('images/arch-top-desktop.jpg'); ?>"/>
    </picture>
</section>
<?php if (!empty($levels = $Galleria->levels)): ?>
    <?php $total_levels = count($levels); ?>
    <section class="galleria-scaffolding container">
        <?php foreach ($levels as $level_index => $level): ?>
            <?php if ($level_index == 1): ?>
                <div class="galleria-scaffolding__level galleria-scaffolding__level--arch">
                    <picture>
                        <source srcset="<?= Util::getAssetPath('images/arch-top-mobile.jpg'); ?>"
                                media="(max-width: 575px)">
                        <img src="<?= Util::getAssetPath('images/arch-top-desktop.jpg'); ?>"/>
                    </picture>
                    <div class="galleria-arch__row">
                        <div class="galleria-arch__pillar">
                            <img src="<?= Util::getAssetPath('images/arch-pillar.jpg'); ?>"/>
                        </div>
                        <div class="galleria-arch__text-frame">
                            <picture>
                                <source srcset="<?= Util::getAssetPath('images/text-frame-mobile.png'); ?>"
                                        media="(max-width: 575px)">
                                <img src="<?= Util::getAssetPath('images/text-frame-desktop.png'); ?>"/>
                            </picture>
                            <div class="galleria-arch__text-container">
                                <?php if (!empty($galleria_text = $Galleria->arch_text)): ?>
                                    <div class="galleria-arch__text">
                                        <?= $galleria_text; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="galleria-arch__pillar">
                            <img src="<?= Util::getAssetPath('images/arch-pillar.jpg'); ?>"/>
                        </div>
                    </div>
                    <picture>
                        <source srcset="<?= Util::getAssetPath('images/arch-top-mobile.jpg'); ?>"
                                media="(max-width: 575px)">
                        <img src="<?= Util::getAssetPath('images/arch-top-desktop.jpg'); ?>"/>
                    </picture>
                </div>
            <?php endif; ?>
            <div
                class="galleria-scaffolding__level galleria-scaffolding__level--<?= ($tier_type = $level['acf_fc_layout']); ?>"
                id="level-<?= $total_levels - $level_index; ?>">
                <?php if ($tier_type == 'gift-guides'): ?>
                    <div class="galleria-scaffolding__title-container">
                        <h2 class="galleria-scaffolding__heading heading heading--large">Shop Our Gift Guides</h2>
                    </div>
                <?php endif; ?>
                <?php if (!empty($windows = $level['items'])): ?>
                    <div class="galleria-scaffolding__windows-container">
                        <?php foreach ($windows as $post_id): ?>
                            <?php if ($tier_type == 'tier-one'): ?>
                                <?= WindowView::tierOneMerchant(new Merchant($post_id)); ?>
                            <?php elseif ($tier_type == 'tier-two'): ?>
                                <?= WindowView::tierTwoMerchant(new Merchant($post_id)); ?>
                            <?php elseif ($tier_type == 'gift-guides'): ?>
                                <?= WindowView::giftGuide(new GiftGuide($post_id)); ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
        <?php if (!empty($lobby_merchants = GlobalOptions::lobbyMerchants())): ?>
            <div class="galleria-scaffolding__level galleria-scaffolding__level--lobby">
                <div class="galleria-scaffolding__windows-container">
                    <div
                        class="galleria-scaffolding__window-awning-container galleria-scaffolding__window-awning-container--first">
                        <div class="galleria-scaffolding__awning-container">
                            <img class="galleria-scaffolding__awning"
                                 src="<?= Util::getAssetPath('images/awning.png'); ?>"/>
                        </div>
                        <?= WindowView::lobby(new Merchant($lobby_merchants[0])); ?>
                    </div>
                    <div
                        class="galleria-scaffolding__window-awning-container galleria-scaffolding__window-awning-container--second">
                        <div class="galleria-scaffolding__awning-container">
                            <img class="galleria-scaffolding__awning"
                                 src="<?= Util::getAssetPath('images/awning.png'); ?>"/>
                        </div>
                        <?= WindowView::lobby(new Merchant($lobby_merchants[1])); ?>
                    </div>
                    <div class="galleria-scaffolding__doorway-container">
                        <img class="galleria-scaffolding__doorway"
                             src="<?= Util::getAssetPath('images/doorway.jpg'); ?>"/>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </section>
<?php endif; ?>
<section class="galleria-sidewalk">
    <picture class="galleria-sidewalk__picture">
        <source srcset="<?= Util::getAssetPath('images/sidewalk-mobile.jpg'); ?>" media="(max-width: 575px)">
        <img class="galleria-sidewalk__image" src="<?= Util::getAssetPath('images/sidewalk.jpg'); ?>"/>
    </picture>
</section>
