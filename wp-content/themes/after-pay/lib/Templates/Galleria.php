<?php

namespace ChildTheme\Templates;

use Backstage\Models\Page;

/**
 * class Galleria
 * @package ChildTheme\Templates
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $arch_text
 * @property array $levels
 */
class Galleria extends Page {}
