<?php

namespace ChildTheme\GiftGuide;

use Backstage\Repositories\PostRepository;

/**
 * Class GiftGuideRepository
 * @package ChildTheme\GiftGuide
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class GiftGuideRepository extends PostRepository
{
    protected $model_class = GiftGuide::class;
}
