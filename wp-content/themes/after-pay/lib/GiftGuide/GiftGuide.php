<?php

namespace ChildTheme\GiftGuide;

use Backstage\Models\PostBase;

/**
 * Class GiftGuide
 * @package ChildTheme\GiftGuide
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $video_source
 * @property string $lobby_video_source
 * @property array $related_merchants
 * @property array $related_gift_guides
 * @property string $descriptive_image
 */
class GiftGuide extends PostBase
{
    const POST_TYPE = 'gift-guide';

    public function descriptiveImage()
    {
        return \WP_Image::get_by_attachment_id($this->field('descriptive_image'));
    }
}
