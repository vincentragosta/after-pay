<?php

namespace ChildTheme\Merchant;

use Backstage\Models\PostBase;

/**
 * Class Merchant
 * @package ChildTheme\Merchant
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $merchant_url
 * @property string $video_source
 * @property string $lobby_video_source
 * @property array $related_merchants
 * @property array $merchant_matrix
 */
class Merchant extends PostBase
{
    const POST_TYPE = 'merchant';

    public function getMerchantsFromMatrix()
    {
        if (empty($matrix = $this->field('merchant_matrix'))) {
            return [];
        }
        if (empty($merchants = array_column($matrix, 'merchant_id'))) {
            return [];
        }
        return array_map(function($merchant_id) {
            return new Merchant($merchant_id);
        }, $merchants);
    }
}
