<?php

namespace ChildTheme\Merchant;

use Backstage\Repositories\PostRepository;

/**
 * Class MerchantRepository
 * @package ChildTheme\Merchant
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class MerchantRepository extends PostRepository
{
    protected $model_class = Merchant::class;
}
