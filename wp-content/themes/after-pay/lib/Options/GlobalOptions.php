<?php

namespace ChildTheme\Options;

use Backstage\Models\OptionsBase;
use Backstage\SetDesign\SocialIcons\SocialIcon;
use WP_Image;

/**
 * Class GlobalOptions
 * @package Orchestrator\Options
 *
 * @method static WP_Image|bool brandLogo()
 * @method static WP_Image|bool logoCondensed()
 * @method static array appleStoreInformation()
 * @method static array googlePlayInformation()
 * @method static array socialIcons()
 * @method static array logInLink()
 * @method static array signUpLink()
 * @method static array labels()
 * @method static string legalText()
 * @method static string searchButtonText()
 * @method static string searchPlaceholderText()
 * @method static array menuItems(string $menu_name)
 * @method static WP_Image|bool appleStoreImage()
 * @method static string appleStoreUrl()
 * @method static WP_Image|bool googlePlayImage()
 * @method static string googlePlayUrl()
 * @method static lobbyMerchants()
 */
class GlobalOptions extends OptionsBase
{
    const MAIN_SITE_URL = 'https://afterpay.com/index';

    protected $default_values = [
        'brand_logo' => null,
        'logo_condensed' => null,
        'apple_store_information' => [],
        'google_play_information' => [],
        'legal_text' => '',
        'social_icons' => [],
        'labels' => [],
        'lobby_merchants' => []
    ];

    protected function getBrandLogo()
    {
        $logo = $this->get('brand_logo');
        return WP_Image::get_by_attachment_id($logo);
    }

    protected function getLogoCondensed()
    {
        $logo_condensed = $this->get('logo_condensed');
        return WP_Image::get_by_attachment_id($logo_condensed);
    }

    protected function getSocialIcons()
    {
        return array_map(function($row) {
            $label = isset($row['link']['title']) ? $row['link']['title'] : ucfirst($row['icon']);
            return new SocialIcon($row['icon'], $row['link']['url'] ?: '', $label, []);
        }, $this->get('social_icons'));
    }

    protected function getSearchButtonText()
    {
        if (empty($labels = $this->labels())) {
            return '';
        }
        return $labels['search'];
    }

    protected function getSearchPlaceholderText()
    {
        if (empty($labels = $this->labels())) {
            return '';
        }
        return $labels['search_placeholder'];
    }

    protected function getMenuItems(string $menu_name)
    {
        if (empty($menus = get_nav_menu_locations())) {
            return [];
        }
        if (empty($menu = $menus[$menu_name])) {
            return [];
        }
        return wp_get_nav_menu_items($menu);
    }

    protected function getAppleStoreImage()
    {
        if (empty($information = $this->appleStoreInformation())) {
            return false;
        }
        return WP_Image::get_by_attachment_id($information['image']);
    }

    protected function getAppleStoreUrl()
    {
        if (empty($information = $this->appleStoreInformation())) {
            return false;
        }
        return $information['url'];
    }

    protected function getGooglePlayImage()
    {
        if (empty($information = $this->googlePlayInformation())) {
            return false;
        }
        return WP_Image::get_by_attachment_id($information['image']);
    }

    protected function getGooglePlayUrl()
    {
        if (empty($information = $this->googlePlayInformation())) {
            return false;
        }
        return $information['url'];
    }

    protected function getLobbyMerchants()
    {
        if (empty($merchants = $this->get('lobby_merchants')) && count($merchants) > 2) {
            return [];
        }
        shuffle($merchants);
        return $merchants;
    }
}
