<?php

namespace ChildTheme;

use Orchestrator\Theme as ThemeBase;
use ChildTheme\Controller;

/**
 * Class Theme
 *
 * Configure settings by overriding parent class constants
 *
 * @package Theme
 */
class Theme extends ThemeBase
{
    const REMOVE_DEFAULT_POST_TYPE = true;

    const NAV_MENUS = [
        'primary_navigation' => 'Primary/Header Navigation',
        'sub_header_navigation' => 'Sub Header Navigation',
        'footer_navigation'  => 'Footer Navigation',
        'sub_footer_navigation' => 'Sub Footer Navigation',
        'sub_footer_navigation_2' => 'Sub Footer Navigation (Second)'
    ];

    const PLATFORM_THEME_SUPPORT = [
        'set-design/nav-menu',
        'design-producer',
    ];

    const EXTENSIONS = [
        Controller\VcLibraryController::class,
        Controller\AmplitudeController::class
    ];

    /**
     * Add theme-specific hooks
     */
    public function __construct()
    {
        parent::__construct();
        add_filter('global_js_vars', [$this, 'updateJsVars'], 10, 1);
        register_nav_menus(static::NAV_MENUS);
    }

    /**
     * Add theme-specific style and script enqueues
     */
    public function assets()
    {
        parent::assets();
    }

    public function updateJSVars($js_vars)
    {
        $args = [];
        if (defined('AMPLITUDE_SDK_ID')) {
            $args['amplitude_sdk_id'] = AMPLITUDE_SDK_ID;
        }
        return array_merge($js_vars, $args);
    }
}
