<?php

namespace ChildTheme\Components\Window;

use Backstage\Models\PostBase;
use Backstage\View\Component;
use Backstage\View\Link;
use ChildTheme\GiftGuide\GiftGuide;
use ChildTheme\Merchant\Merchant;
use ChildTheme\Components\Video\VideoView;

/**
 * Class WindowView
 * @package ChildTheme\Components\Window
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $source
 * @property Link|string $Link
 * @property string $title
 * @property bool $display_window_frame
 * @property \WP_Image|bool|string $mobile_image
 */
class WindowView extends Component
{
    const TIER_ONE_IMAGE_WIDTH = 576;
    const TIER_ONE_IMAGE_HEIGHT = 439;
    const TIER_TWO_IMAGE_WIDTH = 576;
    const TIER_TWO_IMAGE_HEIGHT = 431;
    const GIFT_GUIDE_IMAGE_WIDTH = 576;
    const GIFT_GUIDE_IMAGE_HEIGHT = 576;
    const LOBBY_IMAGE_WIDTH = 576;
    const LOBBY_IMAGE_HEIGHT = 768;

    protected $name = 'window';
    protected static $default_properties = [
        'source' => '',
        'Link' => null,
        'title' => '',
        'display_window_frame' => false,
        'mobile_image' => false
    ];

    public function __construct(string $source, $Link, string $title = '', $mobile_image = '')
    {
        parent::__construct(compact('source', 'Link', 'title', 'mobile_image'));
    }

    public static function tierOneMerchant(Merchant $Merchant)
    {
        $source = '';
        if (!empty($video_source = $Merchant->video_source)) {
            $source = (new VideoView($video_source))->elementAttributes(['class' => 'window__video']);
        } elseif (($image = $Merchant->featuredImage()) instanceof \WP_Image) {
            $source = $image
                ->width(static::TIER_ONE_IMAGE_WIDTH)
                ->height(static::TIER_ONE_IMAGE_HEIGHT)
                ->css_class('window__showcase-image');
        }
        return (new static(
            $source,
            $Merchant->permalink() ? new Link($Merchant->permalink(), 'Explore') : '',
            $Merchant->title(),
            $Merchant->featuredImage()->width(static::TIER_ONE_IMAGE_WIDTH)->height(static::TIER_ONE_IMAGE_HEIGHT)
        ))->windowFrame(true);
    }

    public static function tierTwoMerchant(Merchant $Merchant)
    {
        $source = '';
        if (!empty($video_source = $Merchant->video_source)) {
            $source = (new VideoView($video_source))->elementAttributes(['class' => 'window__video']);
        } elseif (($image = $Merchant->featuredImage()) instanceof \WP_Image) {
            $source = $image
                ->width(static::TIER_TWO_IMAGE_WIDTH)
                ->height(static::TIER_TWO_IMAGE_HEIGHT)
                ->css_class('window__showcase-image');
        }
        return new static(
            $source,
            $Merchant->merchant_url ? new Link($Merchant->merchant_url, 'Shop Now', ['target' => '_blank']) : '',
            $Merchant->title()
        );
    }

    public static function giftGuide(GiftGuide $GiftGuide)
    {
        $source = '';
        if (!empty($video_source = $GiftGuide->video_source)) {
            $source = (new VideoView($video_source))->elementAttributes(['class' => 'window__video']);
        } elseif (($image = $GiftGuide->featuredImage()) instanceof \WP_Image) {
            $source = $image
                ->width(static::GIFT_GUIDE_IMAGE_WIDTH)
                ->height(static::GIFT_GUIDE_IMAGE_HEIGHT)
                ->css_class('window__showcase-image');
        }
        return new static(
            $source,
            $GiftGuide->permalink() ? new Link($GiftGuide->permalink(), 'Explore') : '',
            $GiftGuide->title()
        );
    }

    public static function lobby(PostBase $Post)
    {
        $source = '';
        if (!empty($video_source = $Post->lobby_video_source)) {
            $source = (new VideoView($video_source))->elementAttributes(['class' => 'window__video']);
        } elseif (($image = $Post->featuredImage()) instanceof \WP_Image) {
            $source = $image
                ->width(static::LOBBY_IMAGE_WIDTH)
                ->height(static::LOBBY_IMAGE_HEIGHT)
                ->css_class('window__showcase-image');
        }
        return new static(
            $source,
            $Post->permalink() ? new Link($Post->permalink(), 'Explore') : '',
        );
    }

    public function windowFrame(bool $toggle)
    {
        $this->display_window_frame = $toggle;
        return $this;
    }
}
