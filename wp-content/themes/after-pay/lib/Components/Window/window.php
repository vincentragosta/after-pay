<?php
/**
 * Expected:
 * @var string $source
 * @var Link $Link
 * @var string $title
 * @var bool $display_window_frame
 * @var \WP_Image|bool|string $mobile_image
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;
use Backstage\View\Link;

if (empty($source)) {
    return '';
}
?>

<div <?= Util::componentAttributes('window', $class_modifiers, $element_attributes); ?>>
    <div class="window__showcase-image-container">
        <div class="window__showcase-image-inner">
            <?= $source; ?>
            <?php if ($mobile_image instanceof WP_Image): ?>
                <?= $mobile_image->css_class('window__mobile-image'); ?>
            <?php endif; ?>
        </div>
        <?php if ($Link instanceof Link): ?>
            <div class="window__overlay">
                <?= $Link; ?>
            </div>
        <?php endif; ?>
    </div>
    <?php if ($title): ?>
        <div class="window__title-container">
            <h3 class="window__title"><?= $title; ?></h3>
        </div>
    <?php endif; ?>
    <?php if ($display_window_frame): ?>
        <picture class="window__below-frame-picture">
            <source srcset="<?= Util::getAssetPath('images/window-below-frame-mobile.jpg'); ?>" media="(max-width: 575px)">
            <img src="<?= Util::getAssetPath('images/window-below-frame-desktop.jpg'); ?>" />
        </picture>
    <?php endif; ?>
</div>
