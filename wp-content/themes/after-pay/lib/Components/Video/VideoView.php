<?php

namespace ChildTheme\Components\Video;

use Backstage\View\Component;

/**
 * Class VideoView
 * @package ChildTheme\Components\Video
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $source
 */
class VideoView extends Component
{
    protected $name = 'video';
    protected static $default_properties = [
        'source' => ''
    ];

    public function __construct(string $source)
    {
        parent::__construct(compact('source'));
    }
}
