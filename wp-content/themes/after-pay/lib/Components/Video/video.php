<?php
/**
 * Expected:
 * @var string $source
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;

?>

<video <?= Util::componentAttributes('video', $class_modifiers, $element_attributes); ?> autoplay loop muted>
    <source src="<?= $source; ?>" type="video/mp4">
    Your browser does not support HTML video.
</video>
