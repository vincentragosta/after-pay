<?php

use Backstage\SetDesign\Icon\IconView;
use Backstage\Util;
use ChildTheme\Components\Window\WindowView;
use ChildTheme\GiftGuide\GiftGuide;
use ChildTheme\Merchant\Merchant;
use ChildTheme\Options\GlobalOptions;

$GiftGuide = GiftGuide::createFromGlobal();
if (empty($title = $GiftGuide->title()) || !($image = $GiftGuide->descriptiveImage()) instanceof WP_Image) {
    wp_safe_redirect(home_url());
    exit();
}
?>

<section class="galleria-scaffolding container">
    <div class="galleria-scaffolding__shop-all-container">
        <a href="<?= home_url(); ?>" class="galleria-scaffolding__shop-all"><?= new IconView(['icon_name' => 'long-arrow']); ?> Back To Shop All</a>
    </div>
    <div class="galleria-scaffolding__level galleria-scaffolding__level--hero">
        <div class="galleria-scaffolding__windows-container">
            <div class="window">
                <div class="window__showcase-image-container">
                    <div class="window__showcase-image-inner">
                        <h1 class="single-gift-guide__heading sr-only"><?= $GiftGuide->title(); ?></h1>
                        <?= $image
                            ->width(1280)
                            ->height(575)
                            ->css_class('window__showcase-image'); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($content = $GiftGuide->content(false))): ?>
            <div class="galleria-scaffolding__frame-container">
                <picture>
                    <source srcset="<?= Util::getAssetPath('images/single-gift-guide-columns-mobile.jpg'); ?>" media="(max-width: 575px)">
                    <img src="<?= Util::getAssetPath('images/single-gift-guide-columns-desktop.jpg'); ?>" />
                </picture>
            </div>
        <?php endif; ?>
    </div>
    <?php if (!empty($related_merchants = $GiftGuide->related_merchants)): ?>
        <div class="galleria-scaffolding__level galleria-scaffolding__level--tier-two">
            <div class="galleria-scaffolding__windows-container">
                <?php foreach($related_merchants as $post_id): ?>
                    <?= WindowView::tierTwoMerchant(new Merchant($post_id)); ?>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (!empty($related_gift_guides = $GiftGuide->related_gift_guides)): ?>
        <div class="galleria-scaffolding__related-merchants">
            <div class="galleria-scaffolding__title-container">
                <h2 class="galleria-scaffolding__heading heading heading--medium">You Might Also Like</h2>
            </div>
        </div>
        <div class="galleria-scaffolding__level galleria-scaffolding__level--gift-guides">
            <div class="galleria-scaffolding__windows-container">
                <?php foreach($related_gift_guides as $post_id): ?>
                    <?= WindowView::giftGuide(new GiftGuide($post_id)); ?>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>
<!--    <div class="galleria-scaffolding__level galleria-scaffolding__level--text-columns">-->
<!--        <div class="galleria-scaffolding__row">-->
<!--            <div class="">-->
<!--                <div>-->
<!--                    <h2 class="galleria-scaffolding__heading heading heading--medium">Need Inspiration?</h2>-->
<!--                    <hr />-->
<!--                    <a href="">Shop Gift Guides</a>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="">-->
<!--                <div>-->
<!--                    <p>Shop feel-good gifts that benefit mind, body and soul, from nutrient-packed skincare and wellness essentials to activewear designed to take your fitness routine up a notch.</p>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
    <?php if (!empty($lobby_merchants = GlobalOptions::lobbyMerchants())): ?>
        <div class="galleria-scaffolding__level galleria-scaffolding__level--lobby">
            <div class="galleria-scaffolding__windows-container">
                <div class="galleria-scaffolding__window-awning-container galleria-scaffolding__window-awning-container--first">
                    <div class="galleria-scaffolding__awning-container">
                        <img class="galleria-scaffolding__awning" src="<?= Util::getAssetPath('images/awning.png'); ?>" />
                    </div>
                    <?= WindowView::lobby(new Merchant($lobby_merchants[0])); ?>
                </div>
                <div class="galleria-scaffolding__window-awning-container galleria-scaffolding__window-awning-container--second">
                    <div class="galleria-scaffolding__awning-container">
                        <img class="galleria-scaffolding__awning" src="<?= Util::getAssetPath('images/awning.png'); ?>" />
                    </div>
                    <?= WindowView::lobby(new Merchant($lobby_merchants[1])); ?>
                </div>
                <div class="galleria-scaffolding__doorway-container">
                    <img class="galleria-scaffolding__doorway" src="<?= Util::getAssetPath('images/doorway.jpg'); ?>" />
                </div>
            </div>
        </div>
    <?php endif; ?>
</section>
<section class="galleria-sidewalk">
    <picture class="galleria-sidewalk__picture">
        <source srcset="<?= Util::getAssetPath('images/sidewalk-mobile.jpg'); ?>" media="(max-width: 575px)">
        <img class="galleria-sidewalk__image" src="<?= Util::getAssetPath('images/sidewalk.jpg'); ?>" />
    </picture>
</section>
