addAction(INIT, function() {
    if (!sit.amplitude_sdk_id.length) {
        return;
    }
    amplitude.getInstance().init(sit.amplitude_sdk_id);

    // Global
    $(window).load(function() {
        amplitude.logEvent('Galleria - Page - View', {
            'path': window.location.href,
            'country': 'US'
        });
    });

    // Galleria Template
    var $giftGuideWindows = $('.galleria-scaffolding__level--gift-guides .window');
    if ($giftGuideWindows.length) {
        $.each($giftGuideWindows, function(index, value) {
            var $window = $(value);
            var title = $window.find('.window__title').text();
            var $link = $window.find('.window__overlay > a');
            $link.on('click', function() {
                amplitude.logEvent('Galleria - Gift Guide Link - Click', {
                    'path': window.location.href,
                    'country': 'US',
                    'giftGuideName': title
                });
            });
        });
    }

    // Galleria Template + Single Merchant Template
    var $merchantWindows = $('.page-template-galleria .galleria-scaffolding__level--tier-one .window, .page-template-galleria .galleria-scaffolding__level--tier-two .window, .single-merchant .galleria-scaffolding__level--tier-one .window, .single-merchant .galleria-scaffolding__level--tier-two .window');
    if ($merchantWindows.length) {
        $.each($merchantWindows, function(index, value) {
            var $window = $(value);
            var title = $window.find('.window__title').text();
            var $link = $window.find('.window__overlay > a');
            var properties = {
                'path': window.location.href,
                'country': 'US',
                'merchantName': title
            };
            $link.on('click', function() {
                amplitude.logEvent('Galleria - Merchant Link - Click', properties);
            });
        });
    }

    // Single Merchant Template
    var $merchantProductLinks = $('.featured-overlay__link-container > a');
    if ($merchantProductLinks.length) {
        var title = $('.galleria-scaffolding__heading').text();
        $.each($merchantProductLinks, function(index, value) {
            var $link = $(value);
            var productName = $link.siblings('.featured-overlay__product-title').text();
            $link.on('click', function() {
                amplitude.logEvent('Galleria - Product - Click', {
                    'path': window.location.href,
                    'country': 'US',
                    'merchantName': title,
                    'productName': productName,
                    'outboundLink': $(this).attr('href')
                });
            });
        });
    }

    // Single Gift Guide Template
    var $giftGuideTierTwoWindows = $('.single-gift-guide .galleria-scaffolding__level--tier-two .window');
    if ($giftGuideTierTwoWindows.length) {
        $.each($giftGuideTierTwoWindows, function(index, value) {
            var $window = $(value);
            var title = $window.find('.window__title').text();
            var $link = $window.find('.window__overlay > a');
            var giftGuideName = $('.single-gift-guide__heading').text();
            $link.on('click', function() {
                amplitude.logEvent('Galleria - Store - Click', {
                    'path': window.location.href,
                    'country': 'US',
                    'merchantName': title,
                    'giftGuideName': giftGuideName,
                    'outboundLink': $(this).attr('href')
                });
            });
        });
    }
});
