addAction(INIT, function() {
    var $searchForm = $('#searchform');
    if (!$searchForm.length) {
        return;
    }
    $searchForm.submit(function(e) {
       e.preventDefault();
       var value = $('#s').val();
       if (!value.length) {
           return;
       }
        window.location.href = 'https://www.afterpay.com/stores/search?q=' + value + '&search_version=2';
    });
});
