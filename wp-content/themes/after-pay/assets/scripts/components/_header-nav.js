addAction(INIT, function () {
    var $headerNav = $('.header-nav');
    if (!$headerNav.length) {
        return;
    }
    var $bottomBar = $headerNav.find('.header-nav__bottom-bar');
    var $bottomBarContainer = $bottomBar.find('.header-nav__container');
    var $topBarContainer = $headerNav.find('.header-nav__top-bar .header-nav__container');
    var $logoContainer = $headerNav.find('.header-nav__logo-container');
    var $subMenuContainer = $headerNav.find('.header-nav__sub-menu-container');
    var SHIFT_ACTIVE_CLASS = 'shift-active';
    addAction(LAYOUT, function () {
        var $searchContainer = $headerNav.find('.header-nav__search-container');
        if (!$searchContainer.length) {
            return;
        }
        if ($(window).width() <= 991 && $(window).width() >= 768) {
            $bottomBar.removeClass(SHIFT_ACTIVE_CLASS);
            $searchContainer.appendTo($subMenuContainer);
        } else if ($(window).width() <= 767 && !$bottomBar.hasClass(SHIFT_ACTIVE_CLASS)) {
            $bottomBar.addClass(SHIFT_ACTIVE_CLASS);
            $searchContainer.appendTo($bottomBarContainer);
        } else if ($(window).width() >= 768 && $bottomBar.hasClass(SHIFT_ACTIVE_CLASS) || $(window).width() >= 992) {
            $bottomBar.removeClass(SHIFT_ACTIVE_CLASS);
            if ($logoContainer.length) {
                $searchContainer.insertAfter($logoContainer);
            } else {
                $topBarContainer.prependTo($topBarContainer);
            }
        }
    });
    var $iconArrow = $headerNav.find('.icon-arrow');
    var $iconClose = $headerNav.find('.icon-close');
    if (!$iconArrow.length || !$iconClose.length || !$logoContainer.length) {
        return;
    }
    var $footerNav = $('.footer-nav');
    $iconArrow.click(function () {
        $("html, body").animate({scrollTop: 0}, "slow");
        $logoContainer.addClass('header-nav__logo-container--active');
        $footerNav.addClass('footer-nav--pull-up');
    });
    $iconClose.click(function () {
        $logoContainer.removeClass('header-nav__logo-container--active');
        $footerNav.removeClass('footer-nav--pull-up');
    });
});
