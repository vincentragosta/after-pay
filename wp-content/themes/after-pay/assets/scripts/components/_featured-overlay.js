addAction(INIT, function() {
    var $featuredOverlay = $('.featured-overlay');
    if (!$featuredOverlay.length) {
        return;
    }

    var $button = $('.featured-overlay__button');
    if (!$button.length) {
        return;
    }
    var OPEN_CLASS = 'featured-overlay__modal--open';
    var ACTIVE_CLASS = 'featured-overlay--modal-open';
    $(document).mouseup(function(e) {
        var $modal = $featuredOverlay.find('.featured-overlay__modal');
        var $modalContainer = $modal.find('.featured-overlay__modal-container');
        if (!$modalContainer.is(e.target) && $modalContainer.has(e.target).length === 0) {
            $modal.removeClass(OPEN_CLASS);
            $featuredOverlay.removeClass(ACTIVE_CLASS);
        }
    });
    $button.on('click', function(e) {
        var index = $(this).data('target-index');
        var $modal = $('.featured-overlay__modal[data-index=' + index + ']');
        if ($modal.length) {
            var hasClass = !$modal.hasClass(OPEN_CLASS);
            $modal.toggleClass(OPEN_CLASS, hasClass);
            $featuredOverlay.toggleClass(ACTIVE_CLASS, hasClass);
        }
    });
});
