<?php

use Backstage\SetDesign\Icon\IconView;
use Backstage\Util;
use ChildTheme\Components\Video\VideoView;
use ChildTheme\Components\Window\WindowView;
use ChildTheme\Merchant\Merchant;
use ChildTheme\Options\GlobalOptions;

$Merchant = Merchant::createFromGlobal();
if (empty($title = $Merchant->title()) || !($image = $Merchant->featuredImage()) instanceof WP_Image) {
    wp_safe_redirect(home_url());
    exit();
}
?>

<section class="galleria-scaffolding container">
    <div class="galleria-scaffolding__shop-all-container">
        <a href="<?= home_url(); ?>" class="galleria-scaffolding__shop-all"><?= new IconView(['icon_name' => 'long-arrow']); ?> Back To Shop All</a>
    </div>
    <div class="galleria-scaffolding__level galleria-scaffolding__level--hero">
        <div class="galleria-scaffolding__title-container">
            <h1 class="galleria-scaffolding__heading heading heading--xlarge"><?= $title; ?></h1>
        </div>
        <div class="galleria-scaffolding__windows-container">
            <div class="window">
                <div class="window__showcase-image-container">
                    <div class="window__showcase-image-inner">
                        <?= $image
                            ->width(1280)
                            ->height(656)
                            ->css_class('window__showcase-image'); ?>
                        <?php if (!empty($matrix = $Merchant->merchant_matrix)): ?>
                            <div class="featured-overlay" id="<?= $Merchant->post_name; ?>">
                                <div class="featured-overlay__container">
                                    <?php foreach($matrix as $index => $item): ?>
                                        <?php if (!($Item = new Merchant($item['merchant_id'])) instanceof Merchant): ?>
                                            <?php continue; ?>
                                        <?php endif; ?>
                                        <?php if (empty($Item->merchant_url) || empty($Item->video_source)): ?>
                                            <?php continue; ?>
                                        <?php endif; ?>
                                        <button data-target-index="<?= $index; ?>" class="featured-overlay__button button-unstyled"><?= $item['button_text'] ?: 'Shop Now'; ?></button>
                                        <div class="featured-overlay__modal" data-index="<?= $index; ?>">
                                            <div class="featured-overlay__modal-container">
                                                <div class="featured-overlay__content">
                                                    <?= new VideoView($Item->video_source); ?>
                                                    <div class="featured-overlay__link-container">
                                                        <p class="featured-overlay__product-title sr-only"><?= $Item->title(); ?></p>
                                                        <a href="<?= $Item->merchant_url; ?>" target="_blank">Shop Now</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($content = $Merchant->content(false))): ?>
            <div class="galleria-scaffolding__frame-container">
                <picture>
                    <source srcset="<?= Util::getAssetPath('images/single-hero-frame-mobile.jpg'); ?>" media="(max-width: 575px)">
                    <img src="<?= Util::getAssetPath('images/single-hero-frame-desktop.jpg'); ?>" />
                </picture>
                <div class="galleria-scaffolding__content-container">
                    <?= $content; ?>
                </div>
                <picture>
                    <source srcset="<?= Util::getAssetPath('images/single-hero-columns-mobile.jpg'); ?>" media="(max-width: 575px)">
                    <img src="<?= Util::getAssetPath('images/single-hero-columns-desktop.jpg'); ?>" />
                </picture>
            </div>
        <?php endif; ?>
    </div>
    <?php if (!empty($merchants = $Merchant->getMerchantsFromMatrix())): ?>
        <div class="galleria-scaffolding__level galleria-scaffolding__level--tier-two galleria-scaffolding__level--merchants-matrix">
            <div class="galleria-scaffolding__windows-container">
                <?php foreach($merchants as $Item): ?>
                    <?= WindowView::tierTwoMerchant($Item); ?>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (!empty($related_merchants = $Merchant->related_merchants)): ?>
        <div class="galleria-scaffolding__related-merchants">
            <div class="galleria-scaffolding__title-container">
                <h2 class="galleria-scaffolding__heading heading heading--medium">You Might Also Like</h2>
            </div>
        </div>
        <div class="galleria-scaffolding__level galleria-scaffolding__level--tier-two">
            <div class="galleria-scaffolding__windows-container">
                <?php foreach($related_merchants as $post_id): ?>
                    <?= WindowView::tierTwoMerchant(new Merchant($post_id)); ?>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (!empty($lobby_merchants = GlobalOptions::lobbyMerchants())): ?>
        <div class="galleria-scaffolding__level galleria-scaffolding__level--lobby">
            <div class="galleria-scaffolding__windows-container">
                <div class="galleria-scaffolding__window-awning-container galleria-scaffolding__window-awning-container--first">
                    <div class="galleria-scaffolding__awning-container">
                        <img class="galleria-scaffolding__awning" src="<?= Util::getAssetPath('images/awning.png'); ?>" />
                    </div>
                    <?= WindowView::lobby(new Merchant($lobby_merchants[0])); ?>
                </div>
                <div class="galleria-scaffolding__window-awning-container galleria-scaffolding__window-awning-container--second">
                    <div class="galleria-scaffolding__awning-container">
                        <img class="galleria-scaffolding__awning" src="<?= Util::getAssetPath('images/awning.png'); ?>" />
                    </div>
                    <?= WindowView::lobby(new Merchant($lobby_merchants[1])); ?>
                </div>
                <div class="galleria-scaffolding__doorway-container">
                    <img class="galleria-scaffolding__doorway" src="<?= Util::getAssetPath('images/doorway.jpg'); ?>" />
                </div>
            </div>
        </div>
    <?php endif; ?>
</section>
<section class="galleria-sidewalk">
    <picture class="galleria-sidewalk__picture">
        <source srcset="<?= Util::getAssetPath('images/sidewalk-mobile.jpg'); ?>" media="(max-width: 575px)">
        <img class="galleria-sidewalk__image" src="<?= Util::getAssetPath('images/sidewalk.jpg'); ?>" />
    </picture>
</section>
