<?php
define( 'WP_CACHE', false ); // Added by WP Rocket


// ==============================
// Composer autoloader if present
// ==============================
if (file_exists(__DIR__ . '/wp-content/vendor/autoload.php')) {
    define('USE_COMPOSER_AUTOLOADER', true);
    require_once __DIR__ . '/wp-content/vendor/autoload.php';
}

// ===================================================
// Load database info and local development parameters
// ===================================================
if (file_exists(__DIR__ . '/local-config.php')) {
    include(__DIR__ . '/local-config.php');
}

// ===================================================
// Initialize Situation defaults
// ===================================================
if (class_exists('\Situation\WPConfig')) {
    new \Situation\WPConfig(__DIR__);
}

// ================================================
// You almost certainly do not want to change these
// ================================================
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');

// ==============================================================
// Salts, for security
// Grab these from: https://api.wordpress.org/secret-key/1.1/salt
// ==============================================================
define('AUTH_KEY',         'CO&nt|B@9Ut9X.9*LGiN?@aikzsFQd}69zMe&I 7M!6_8;W6:;B_Y Qz14*h&AN?');
define('SECURE_AUTH_KEY',  '3Tzs2: e!{rW}E]A3+i;2NX5J+fAy.?,-i8tJG,).:mP]8|6;(lG+|DcNq+S B2}');
define('LOGGED_IN_KEY',    'pm:UGVZG--+CR!/5w}lC]_((+2W</JeBwurtIz?{9+vnS0ZO>7/ziC0e0F&XkqT_');
define('NONCE_KEY',        'DI||wTp?mvuezw)/<tDwK%k_HnHt%K9CL[PjG.gp{jnTaqW(96LHx8#ax7SM8Dd^');
define('AUTH_SALT',        'Fpf/+S+RCKk.B%Z:6%E*oeyy<+ZZO8q_m$%7hmy6>Su/tU%xxn+1+ep:,<*Q@`$^');
define('SECURE_AUTH_SALT', 'Bgy|0wa()=IV.WSU:[4&14Y]w#2k)OwWGv/TQZKhg3VPbv;CPk+q+<xWB+6f;ye_');
define('LOGGED_IN_SALT',   'pSU-/l;y=F]9d(LLcM-z8YA T|(9@^jj`# Pf+7|q*2w!P<Gxg2/||kq-T$Y|,d*');
define('NONCE_SALT',       'T+u<%q.Tf+M `~F[$|-LLDddQmL_c]3-[:gY:8F=f{jIp+5Tdlx6DUO sV6.~^q~');


// ==============================================================
// Table prefix
// Change this if you have multiple installs in the same database
// Changing default to 'sit_' to enhance security
// ==============================================================
$table_prefix = 'sit_';

// ================================
// Language
// Leave blank for American English
// ================================
define('WPLANG', '');

// ===================
// Bootstrap WordPress
// ===================
if (! defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/wp/');
}
require_once(ABSPATH . 'wp-settings.php');
